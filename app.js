var _ = require('lodash');
var commander = require('commander');
var util = require('util')
var jf = require('jsonfile');
var fs = require('fs');

var testFolder = './tests/';

var currentTripPath = './data/paris.json';
var trip = jf.readFileSync(currentTripPath);

if (process.argv[2] == 'test') {
    return runTests();
}

/* APP */

function normalizePayments(receipt) {
    if (typeof receipt.payments == 'string') {
        var person = receipt.payments;
        receipt.payments = { };
        receipt.payments[person] = 'rest';
    }
}

function normalizeEntriesPrice(receipt) {
    _.forEach(receipt.entries, function (entry) {
        if (entry.price != 'rest') {
            entry.price = entry.price * (entry.quantity || 1);
        }
        entry.quantity = 1;
    });
}

function normalizeCcy(receipt, ccy) {
    if (receipt.currency != ccy.base) {
        _.forEach(receipt.payments, function (value, person) {
            if (receipt.payments[person] != 'rest') {
                receipt.payments[person] = value * ccy.rates[receipt.currency];
            }
        });
        _.forEach(receipt.entries, function (entry) {
            if (entry.price != 'rest') {
                entry.price = entry.price * ccy.rates[receipt.currency];    
            }
        });
    }
}

function computeTotal(receipt) {
    if (_.values(receipt.payments).indexOf('rest') < 0) {
        receipt.total = _.sum(_.values(receipt.payments));
    } else {
        receipt.total = _.sum(_.map(receipt.entries, function (entry) {
            return entry.price;
        }));
    }
}

function computeRest(receipt) {
    var restIndex = null, oldRestIndex = null;
    var sumSoFar = 0;

    _.forEach(receipt.payments, function (payment, person) {
        if (payment == 'rest') {
            if (restIndex != null) {
                throw new Error('A receipt cannot have multiple rest entries.');
            }
            restIndex = person;
        } else {
            sumSoFar += payment;
        }
    });

    if (restIndex) {
        receipt.payments[restIndex] = receipt.total - sumSoFar;
    }

    sumSoFar = 0; oldRestIndex = restIndex;

    _.forEach(receipt.entries, function (entry, index) {
        if (entry.price == 'rest') {

            if (restIndex != null) {
                throw new Error('A receipt cannot have multiple rest entries.');
            }
            restIndex = index;
        } else {
            sumSoFar += entry.price;
        }
    });

    if (restIndex != oldRestIndex) {
        receipt.entries[restIndex].price = receipt.total - sumSoFar;
        receipt.entries[restIndex].quantity = 1;
        // receipt.entries[restIndex].total = receipt.entries[restIndex].price;
    }
}

function computeIndividualExpenses(trip) {
    var persons = _.values(trip.persons).length;
    _.forEach(trip.persons, function (person, personId) {
        person.payments = _.round(_.sum(_.map(trip.receipts, function (receipt) {
            return receipt.payments[personId] || 0;
        })), 2);
        person.expenses = _.round(_.sum(_.flatMap(trip.receipts, function (receipt) {
            return _.map(receipt.entries, function (entry) {
                if (entry.relevance == 'split') {
                    return entry.price / persons;
                }

                if (entry.relevance == personId) {
                    return entry.price;
                }

                return 0;
            });
        })), 2);
        person.debt = person.payments - person.expenses;
        if (person.debt > 0) {
            person.receives = _.round(Math.abs(person.debt), 2);
        } else if (person.debt) {
            person.ows = _.round(Math.abs(person.debt), 2);
        }
        delete person.debt;
    });
}

function computeIndividualDebts(trip) {
    var persons = _.values(trip.persons).length;
    _.forEach(trip.persons, function (person, personId) {
        person.debt = 0;
        person.receives = 0;
        person.ows = 0;
        _.forEach(trip.receipts, function(receipt) {
            var personPayment = receipt.payments[personId] || 0;
            _.forEach(receipt.entries, function(entry) {
                if (entry.relevance == personId) {
                    personPayment -= entry.price;
                } 
                if (entry.relevance == 'split') {
                    personPayment -= (entry.price / persons);
                }
            });
            // console.log(personId + ',' + receipt.summary + ',' + personPayment);
            person.debt += personPayment;
            console.log(personId + ' debt ' + person.debt + ' for ' + receipt.summary);
        });

        if (person.debt > 0) {
            person.receives += person.debt;
        } else if (person.debt < 0) {
            person.ows += person.debt;
        }
        person.receives = _.round(Math.abs(person.receives), 2);
        person.ows = _.round(Math.abs(person.ows), 2);
        delete person.debt;
    });
}

function performTripExpenses(trip) {
    var trip = _.clone(trip);
    
    _.forEach(trip.receipts, function (receipt) {
        normalizePayments(receipt);
        normalizeEntriesPrice(receipt);
        normalizeCcy(receipt, trip.currency);
        computeTotal(receipt);
        computeRest(receipt);
    });

    computeIndividualExpenses(trip);
    // computeIndividualDebts(trip);

    return trip;
}

// TESTS
function runTests() {
    fs.readdir(testFolder, function (err, files) {
        _.forEach(files, function(file, idx) {
            jf.readFile(testFolder + file, function(err, demoTrip) {
                console.log('Test ' + idx);
                console.log(util.inspect(performTripExpenses(demoTrip).persons, false, null));
            });
        });
    });    
}

// RUN
var expenses = performTripExpenses(trip);
console.log(util.inspect(expenses.persons, false, null));